country flags
=============
country flags

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist duongnh/yii2-country-flags "*"
```

or add

```
"duongnh/yii2-country-flags": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \duongnh\country\flags\AutoloadExample::widget(); ?>```